<?php

namespace Nitra\ReviewBundle\Form\Type\Review;

use Admingenerated\NitraReviewBundle\Form\BaseReviewType\FiltersType as BaseFiltersType;
use Symfony\Component\Form\FormBuilderInterface;

class FiltersType extends BaseFiltersType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder->add('buyer', 'genemu_jqueryautocompleter_document', array(
            'class'                 => 'Nitra\BuyerBundle\Document\Buyer',
            'property'              => 'name',
            'required'              => false,
            'label'                 => 'buyer_name',
            'translation_domain'    => 'NitraReviewBundle'
        ));
    }
}