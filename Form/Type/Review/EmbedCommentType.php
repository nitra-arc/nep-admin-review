<?php

namespace Nitra\ReviewBundle\Form\Type\Review;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;

class EmbedCommentType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($options) {
            $form = $event->getForm();
            $data = $event->getData();

            if ($data) {
                $nameFromBuyer = $data->getBuyer() ? $data->getBuyer()->getName() : '';
                $buyerName     = $data->getUserName() ? $data->getUserName() : $nameFromBuyer;
                $sendEmail     = false;
            } else {
                $buyerName      = $options['managerName'];
                $sendEmail      = true;
            }

            $form->add('message', 'textarea', array(
                'required'  => false,
                'label'     => 'message',
            ));
            $form->add('status', 'choice', array(
                'data'      => $data ? $data->getStatus() : true,
                'required'  => true,
                'choices'   => array(
                    1           => 'active',
                    0           => 'off',
                ),
                'label'     => 'status',
            ));
            $form->add('moderated', 'choice', array(
                'data'      => $data ? $data->getModerated() : true,
                'required'  => true,
                'choices'   => array(
                    0           => 'moderated.val_false',
                    1           => 'moderated.val_true',
                ),
                'label'     => 'moderate',
            ));
            $form->add('buyerName', 'text', array(
                'data'      => $buyerName,
                'mapped'    => false,
                'read_only' => true,
                'label'     => 'name'
            ));
            $form->add('userName', 'hidden', array(
                'data'      => $buyerName,
            ));
            $form->add('sendEmail', 'hidden', array(
                'data'      => $sendEmail,
            ));
        });
    }

    public function getName()
    {
        return 'review_answer';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'         => 'Nitra\\ReviewBundle\\Document\\EmbedComent',
            'managerName'        => array(),
            'translation_domain' => 'NitraReviewBundle',
        ));
    }
}