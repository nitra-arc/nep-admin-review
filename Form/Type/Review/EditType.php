<?php

namespace Nitra\ReviewBundle\Form\Type\Review;

use Admingenerated\NitraReviewBundle\Form\BaseReviewType\EditType as BaseEditType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EditType extends BaseEditType
{
    private $options = array();
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->options = $options;
        parent::buildForm($builder, $options);
    }
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'managerName' => array(),
        ));
    }
    protected function getFormOption($name, array $formOptions)
    {
        if ($name == 'embedded_coment') {
            if (key_exists('options', $formOptions)) {
                $formOptions['options']['managerName'] = $this->options['managerName'];
            } else {
                $formOptions['options'] = array('managerName' => $this->options['managerName']);
            }
        }
        return $formOptions;
    }
}