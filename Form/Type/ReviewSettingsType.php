<?php

namespace Nitra\ReviewBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ReviewSettingsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('show_unmoderated', 'choice', array(
            'label'         => 'settings.showUnmoderatedLabel',
            'required'      => true,
            'empty_value'   => '',
            'choices'       => array(
                1               => 'yes',
                0               => 'no',
            ),
        ));
        $builder->add('sendEmail', 'choice', array(
            'label'         => 'settings.sendEmailLabel',
            'required'      => true,
            'empty_value'   => '',
            'choices'       => array(
                1               => 'yes',
                0               => 'no',
            ),
        ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'         => 'Nitra\\ReviewBundle\\Document\\ReviewSettings',
            'translation_domain' => 'NitraReviewBundle',
        ));
    }

    public function getName()
    {
        return 'review_settings';
    }
}