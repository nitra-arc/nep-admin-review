ReviewBundle
==============
предназначен для:

* **управления отзывами**

Enable the bundle
=================

Enable the bundle in the kernel:

```php
// app/AppKernel.php

public function registerBundles()
{
    $bundles = array(
        // ...
       
       new Nitra\ReviewBundle\NitraReviewBundle(),
    );
}
```

Enable the bundle in the config.yml:

```yml
genemu_form:
    autocomplete: ~
    autocompleter:
        mongodb: true
    select2: ~
    date: ~
```

Import the routing
===================

Import the routing

```yaml
# app/config/routing.yml

NitraReviewBundle:
    resource:    "@NitraReviewBundle/Resources/config/routing.yml"
    prefix:      /{_locale}/
    defaults:
        _locale: ru
    requirements:
        _locale: en|ru
```

Add in menu
===========


```yaml
items: #menu items
    # ...
    reviewTable:
        translateDomain: 'menu'
        route: Nitra_ReviewBundle_Review_list
    # ...
    reviewSettings:
        translateDomain: 'menu'
        route: NitraReviewBundle_Settings
    # ...
```