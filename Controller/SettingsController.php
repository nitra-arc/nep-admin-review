<?php

namespace Nitra\ReviewBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Nitra\ReviewBundle\Document\ReviewSettings;
use Nitra\ReviewBundle\Form\Type\ReviewSettingsType;

class SettingsController extends Controller
{
    /**
     * @Template("NitraReviewBundle:ReviewSettings:index.html.twig")
     * @Route("/", name="NitraReviewBundle_Settings")
     */
    public function indexAction(Request $request)
    {
        $Settings = $this->getSettings();
        $form = $this->getReviewSettingsForm($Settings);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDocumentManager()->flush();
            $this->get('session')->getFlashBag()->add('success', 'Настройки сохранены');
            return new RedirectResponse($this->generateUrl('NitraReviewBundle_Settings'));
        }

        return array(
            'baseTemplate' => $this->container->getParameter('admingenerator.base_admin_template'),
            'form'         => $form->createView(),
        );
    }

    /** @return \Doctrine\ODM\MongoDB\DocumentManager */
    protected function getDocumentManager() { return $this->container->get('doctrine_mongodb.odm.document_manager'); }

    /**
     * @param \Nitra\ReviewBundle\Document\ReviewSettings $Settings
     * @return \Symfony\Component\Form\Form
     */
    protected function getReviewSettingsForm($Settings)
    {
        return $this->createForm(new ReviewSettingsType(), $Settings, array(
            'action'    => $this->generateUrl('NitraReviewBundle_Settings'),
            'attr'      => array(
                'class'     => 'form_admin',
            ),
        ));
    }

    /**
     * @return \Nitra\ReviewBundle\Document\ReviewSettings
     */
    protected function getSettings()
    {
        $Settings = $this->getDocumentManager()->getRepository('NitraReviewBundle:ReviewSettings')->findOneBy(array());
        if (!$Settings) {
            $Settings = new ReviewSettings();
            $this->getDocumentManager()->persist($Settings);
        }

        return $Settings;
    }
}