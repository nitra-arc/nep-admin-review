<?php

namespace Nitra\ReviewBundle\Controller\Review;

use Admingenerated\NitraReviewBundle\BaseReviewController\EditController as BaseEditController;

class EditController extends BaseEditController
{
    public function preSave(\Symfony\Component\Form\Form $form, \Nitra\ReviewBundle\Document\Review $Review)
    {  
        $dm = $this->getDocumentManager();
        $settings = $dm->getRepository('NitraReviewBundle:ReviewSettings')->findOneBy(array());
        $sendEmail = $settings->getSendEmail();
        $unmoderatedAnswer = true;
        $embeddedComent = $Review->getEmbeddedComent();
        foreach ($embeddedComent as $value) {
            if (!($value->getModerated())) {
                $unmoderatedAnswer = false;
            }
        }
        $Review->setAnswerModerated($unmoderatedAnswer);

        foreach ($embeddedComent as $value) {
            $email = $Review->getBuyer() ? $Review->getBuyer()->getEmail() : null;
            if ($value->getSendEmail() && $sendEmail && $email) {
                $reviewText = $Review->getMessage();
                $userName = $Review->getUserName()?$Review->getUserName():$Review->getBuyer()->getName();
                $this->sendEmail($userName, $reviewText, $value, $email);
            }
        }
    }

    protected function getFormOptions(\Nitra\ReviewBundle\Document\Review $Review)
    {
        $managerName = $this->getManager();

        return array(
            'managerName' => $managerName,
        );
    }

    protected function getManager()
    {
        $usr = $this->get('security.context')->getToken()->getUser();
        $managerName = $usr->getLastname() . " " . $usr->getFirstname();
        return $managerName;
    }

    private function sendEmail($userName, $reviewText, $answer, $email)
    {
        $session = $this->getRequest()->getSession();
        $dm = $this->getDocumentManager();
        $store = $dm->getRepository('NitraStoreBundle:Store')->findOneById($session->get('store_id'));
        $mailingEmail = $store->getMailingEmail();
        $subject = $this->get('translator')->trans("mailing.mailingSubject", array(), 'NitraReviewBundle') . ' ' . $store->getName();
        $message = \Swift_Message::newInstance()
            ->setSubject($subject)
            ->setFrom($mailingEmail)
            ->setTo($email)
            ->setContentType('text/html')
            ->setBody($this->renderView('NitraReviewBundle:ReviewEdit:sendMail.html.twig', array(
                'reviewText'    => $reviewText,
                'answerText'    => $answer->getMessage(),
                'storeName'     => $store->getName(),
                'managerName'   => $answer->getUserName(),
                'userName'      => $userName,
            )));
        $this->get('mailer')->send($message);
    }
}