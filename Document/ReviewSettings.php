<?php

namespace Nitra\ReviewBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ODM\Document 
 */
class ReviewSettings
{
    /**
     * @ODM\Id(strategy="AUTO")
     */
    private $id;

    /**
     * @ODM\Boolean 
     * @Assert\NotBlank
     */
    private $show_unmoderated;

    /**
     * @ODM\Boolean 
     * @Assert\NotBlank
     */
    private $sendEmail;

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set showUnmoderated
     *
     * @param boolean $showUnmoderated
     * @return self
     */
    public function setShowUnmoderated($showUnmoderated)
    {
        $this->show_unmoderated = $showUnmoderated;
        return $this;
    }

    /**
     * Get showUnmoderated
     *
     * @return boolean $showUnmoderated
     */
    public function getShowUnmoderated()
    {
        return $this->show_unmoderated;
    }

    /**
     * Set sendEmail
     *
     * @param boolean $sendEmail
     * @return self
     */
    public function setSendEmail($sendEmail)
    {
        $this->sendEmail = $sendEmail;
        return $this;
    }

    /**
     * Get sendEmail
     *
     * @return boolean $sendEmail
     */
    public function getSendEmail()
    {
        return $this->sendEmail;
    }
}