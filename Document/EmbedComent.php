<?php

namespace Nitra\ReviewBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ODM\EmbeddedDocument
 */
class EmbedComent
{
    use \Gedmo\Timestampable\Traits\TimestampableDocument;
    use \Gedmo\Blameable\Traits\BlameableDocument;

    /**
     * @ODM\Id(strategy="AUTO")
     */
    private $id;

    /**
     * @ODM\ReferenceOne(targetDocument="Nitra\BuyerBundle\Document\Buyer")
     */
    private $buyer;

    /**
     * @ODM\String
     */
    private $message;

    /**
     * @ODM\Boolean
     */
    private $status;

    /**
     * @ODM\Boolean
     */
    private $sendEmail;

    /**
     * @ODM\Boolean
     */
    private $moderated;

    /**
     * @ODM\Date
     */
    private $add_date;

    /**
     * @ODM\String
     */
    private $userName;

    public function __construct()
    {
        $mongoDateObject = new \MongoDate();
        $this->setAddDate($mongoDateObject);
    }

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set message
     *
     * @param string $message
     * @return self
     */
    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    /**
     * Get message
     *
     * @return string $message
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set status
     *
     * @param boolean $status
     * @return self
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * Get status
     *
     * @return boolean $status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set moderated
     *
     * @param boolean $moderated
     * @return self
     */
    public function setModerated($moderated)
    {
        $this->moderated = $moderated;
        return $this;
    }

    /**
     * Get moderated
     *
     * @return boolean $moderated
     */
    public function getModerated()
    {
        return $this->moderated;
    }

    /**
     * Set buyer
     *
     * @param Nitra\BuyerBundle\Document\Buyer $buyer
     * @return self
     */
    public function setBuyer(\Nitra\BuyerBundle\Document\Buyer $buyer)
    {
        $this->buyer = $buyer;
        return $this;
    }

    /**
     * Get buyer
     *
     * @return Nitra\BuyerBundle\Document\Buyer $buyer
     */
    public function getBuyer()
    {
        return $this->buyer;
    }

    /**
     * Set addDate
     *
     * @param date $addDate
     * @return self
     */
    public function setAddDate($addDate)
    {
        $this->add_date = $addDate;
        return $this;
    }

    /**
     * Get addDate
     *
     * @return date $addDate
     */
    public function getAddDate()
    {
        return $this->add_date;
    }

    /**
     * Set userName
     *
     * @param string $userName
     * @return self
     */
    public function setUserName($userName)
    {
        $this->userName = $userName;
        return $this;
    }

    /**
     * Get userName
     *
     * @return string $userName
     */
    public function getUserName()
    {
        return $this->userName;
    }

    /**
     * Set sendEmail
     *
     * @param boolean $sendEmail
     * @return self
     */
    public function setSendEmail($sendEmail)
    {
        $this->sendEmail = $sendEmail;
        return $this;
    }

    /**
     * Get sendEmail
     *
     * @return boolean $sendEmail
     */
    public function getSendEmail()
    {
        return $this->sendEmail;
    }
}